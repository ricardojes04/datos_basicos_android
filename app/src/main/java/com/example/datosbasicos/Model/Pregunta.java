package com.example.datosbasicos.Model;

public class Pregunta {

    private Integer id_pregunta;
    private String pregunta;
    private Boolean respuesta;

    public Pregunta() {
    }

    public Pregunta(Integer id_pregunta, String pregunta, Boolean respuesta) {
        this.id_pregunta = id_pregunta;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
    }

    public Integer getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(Integer id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }
}
