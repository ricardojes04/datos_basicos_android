package com.example.datosbasicos.Model;

public class Usuario {

    private Integer id;
    private String user;
    private String clave;
    private String identificacion;
    private String nombre;
    private String apellido;
    private Persona persona;

    public Usuario(Integer id, String user, String clave, String identificacion, String nombre, String apellido, Persona persona) {
        this.id = id;
        this.user = user;
        this.clave = clave;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.persona = persona;
    }

    public Usuario() {
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
