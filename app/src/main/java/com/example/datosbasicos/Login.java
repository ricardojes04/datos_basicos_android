package com.example.datosbasicos;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.datosbasicos.Model.Persona;

public class Login extends AppCompatActivity {

    EditText text_email;
    EditText text_pass;
    private String CREDENTIALS;
    private static final String KEY_USER = "user_name";
    private static final String KEY_PASS = "user_pass";
    private static final String KEY_OK_SESSION = "ok_session";
    Boolean okSession = Boolean.FALSE;

    public Persona persona = new Persona("Grupo F", "Electiva",
            19, "", "grupoF@gmail.com", "123", null, null, null, null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final Button btton_login = (Button) findViewById(R.id.btn_login);
        final Button btton_new_user = (Button) findViewById(R.id.btn_new_user);
        text_email = (EditText) findViewById(R.id.input_email);
        text_pass = (EditText) findViewById(R.id.input_password);
        CREDENTIALS = getString(R.string.credentials);
        loadPreferences();
        if (okSession) {
            intentActivity();
        }
        btton_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar()) {
                    login();
                }
            }
        });
        btton_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_contacto:
                Toast.makeText(getBaseContext(), "Selecciono el menu contacto", Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_item_idioma:
                Toast.makeText(getBaseContext(), "Selecciono el menu idioma", Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_item_exit:
                Toast.makeText(getBaseContext(), "Selecciono el menu exit", Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_item_acerdade:
                Intent intent = new Intent(getApplicationContext(), AcercaDeActivity.class);
                //intent.putExtra("name_user", persona.getNombre());
                startActivityForResult(intent, 0);
                break;
        }
        return true;
    }

    public void login() {
        if (text_email.getText().toString().equals(persona.getEmail()) && text_pass.getText().toString().equals(persona.getPassword())) {
            alertDialog();
        } else {
            Toast.makeText(getBaseContext(), "El usuario o contraseña incorrecta", Toast.LENGTH_LONG).show();
        }
    }

    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("¡Sesión!");
        builder.setMessage("¿Desea guardar la sesión?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                savePreferences();
                intentActivity();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                intentActivity();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void intentActivity() {
        Toast.makeText(getBaseContext(), "Bienvenido " + persona.getPrimerNombrePrimerApellido(), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getApplicationContext(), PreguntaActivity.class);
        intent.putExtra("name_user", persona.getNombre());
        startActivityForResult(intent, 0);
    }

    public Boolean validar() {
        if (text_email.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese un email", Toast.LENGTH_LONG).show();
            return false;
        }
        if (text_pass.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese el password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void savePreferences() {
        okSession = Boolean.TRUE;
        SharedPreferences preferences = getSharedPreferences(CREDENTIALS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USER, text_email.getText().toString());
        editor.putString(KEY_PASS, text_pass.getText().toString());
        editor.putBoolean(KEY_OK_SESSION, okSession);
        editor.commit();
    }

    private void loadPreferences() {
        SharedPreferences preferences = getSharedPreferences(CREDENTIALS, Context.MODE_PRIVATE);
        text_email.setText(preferences.getString(KEY_USER, ""));
        text_pass.setText(preferences.getString(KEY_PASS, ""));
        okSession = preferences.getBoolean(KEY_OK_SESSION, Boolean.FALSE);
    }
}