package com.example.datosbasicos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.datosbasicos.Model.Pregunta;

import org.w3c.dom.Text;

public class PreguntaActivity extends AppCompatActivity {

    public int puntaje = 0;
    public int indexPregunta;
    private String CREDENTIALS;
    public Pregunta preguntas[] = {
            new Pregunta(1, "¿Estamos solos en el universo?", false),
            new Pregunta(2, "¿Todos van a pasar el curso?", false),
            new Pregunta(3, "¿El examen va a ser dificil?", true)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregunta);

        final TextView textPregunta = (TextView) findViewById(R.id.text_pregunta);
        final TextView textPuntaje = (TextView) findViewById(R.id.text_puntaje);
        TextView textNameUser = (TextView) findViewById(R.id.text_name_user);
        textPuntaje.setText(String.valueOf(puntaje));
        textPregunta.setText(String.valueOf(preguntas[0].getPregunta()));
        indexPregunta = preguntas[0].getId_pregunta();
        CREDENTIALS = getString(R.string.credentials);

        Button buttonTrue = (Button) findViewById(R.id.buttom_true);
        Button buttonFalse = (Button) findViewById(R.id.buttom_false);
        Button buttonClean = (Button) findViewById(R.id.button_borrar_datos);
        Button buttonSearch = (Button) findViewById(R.id.button_search_datos);

        Bundle bundle = getIntent().getExtras();
        textNameUser.setText(bundle.getString("name_user"));

        buttonTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarReply(true);
                textPuntaje.setText(String.valueOf(puntaje));
                refreshPregunta(textPregunta);
            }
        });

        buttonFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarReply(false);
                textPuntaje.setText(String.valueOf(puntaje));
                refreshPregunta(textPregunta);
            }
        });
        buttonClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchUserActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    public void validarReply(Boolean respuesta) {
        for (int i = 0; i < preguntas.length; i++) {
            if (preguntas[i].getId_pregunta() == indexPregunta) {
                if (preguntas[i].getRespuesta().equals(respuesta)) {
                    puntaje = puntaje + 10;
                } else {
                    puntaje = puntaje - 10;
                }
            }
        }
    }

    public void refreshPregunta(TextView pregunta) {
        indexPregunta++;
        if (indexPregunta <= preguntas.length) {
            pregunta.setText(preguntas[indexPregunta - 1].getPregunta());
        } else {
            pregunta.setText(preguntas[0].getPregunta());
        }
    }

    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("¡Alerta!");
        builder.setMessage("¿Está seguro de eliminar los datos de la sesión?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences preferences = getSharedPreferences(CREDENTIALS, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivityForResult(intent, 0);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}