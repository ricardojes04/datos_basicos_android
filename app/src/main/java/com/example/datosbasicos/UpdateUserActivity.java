package com.example.datosbasicos;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.datosbasicos.Model.Usuario;
import com.example.datosbasicos.SQLLite.MyOpenHelper;


public class UpdateUserActivity extends AppCompatActivity {

    private MyOpenHelper myOpenHelper;
    private String id_user;
    private EditText editText_nombre;
    private EditText editText_apellido;
    private EditText editText_identificacion;
    private EditText editText_email;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);

        //OBTENIENDO EL ID DEL USUARIO O PERSONA A EDITAR
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id_user = bundle.getString("identificacion_user");
        System.out.println("identificacion update" + id_user);

        editText_nombre = (EditText) findViewById(R.id.input_update_nombre);
        editText_apellido = (EditText) findViewById(R.id.input_update_apellido);
        editText_identificacion = (EditText) findViewById(R.id.input_update_identificacion);
        editText_email = (EditText) findViewById(R.id.input_update_user);

        Button button_update = (Button) findViewById(R.id.btn_update_user);
        Button button_cancel = (Button) findViewById(R.id.btn_cancel_update);


        //CREANDO MY OPEN HELPER
        myOpenHelper = new MyOpenHelper(this);
        db = myOpenHelper.getWritableDatabase();
        if (db != null && id_user != null && !id_user.isEmpty()) {
            String[] args = new String[]{id_user};
            Cursor c = db.rawQuery("SELECT u.nombre, u.apellido, u.identificacion, u.user" +
                    " FROM usuario u WHERE u.identificacion =?", args);
            if (c != null) {
                c.moveToFirst();
                do {
                    editText_nombre.setText(c.getString(c.getColumnIndex("nombre")));
                    editText_apellido.setText(c.getString(c.getColumnIndex("apellido")));
                    editText_identificacion.setText(c.getString(c.getColumnIndex("identificacion")));
                    editText_email.setText(c.getString(c.getColumnIndex("user")));
                } while (c.moveToNext());
            }
            c.close();
            db.close();
        }

        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar()) {
                    db = myOpenHelper.getWritableDatabase();
                    String[] args = new String[]{editText_identificacion.getText().toString()};
                    ContentValues cv = new ContentValues();
                    cv.put("nombre", editText_nombre.getText().toString());
                    cv.put("apellido", editText_apellido.getText().toString());
                    cv.put("identificacion", editText_identificacion.getText().toString());
                    cv.put("user", editText_email.getText().toString());
                    db.update("usuario", cv, "identificacion=?", args);
                    db.close();
                    Toast.makeText(getBaseContext(), "Usuario " + editText_nombre.getText().toString() +
                            " Actualizado \uD83D\uDE01", Toast.LENGTH_LONG).show();
                }
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SearchUserActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    private Boolean validar() {
        if (editText_nombre.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese un nombre \uD83D\uDE41", Toast.LENGTH_LONG).show();
            return false;
        }
        if (editText_apellido.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese un apellido \uD83D\uDE41", Toast.LENGTH_LONG).show();
            return false;
        }
        if (editText_identificacion.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese una identificación \uD83D\uDE41", Toast.LENGTH_LONG).show();
            return false;
        }
        if (editText_email.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese un email \uD83D\uDE41", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}
