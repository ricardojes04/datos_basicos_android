package com.example.datosbasicos;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datosbasicos.Fragment.DatePickerFragment;
import com.example.datosbasicos.Model.Persona;
import com.example.datosbasicos.SQLLite.MyOpenHelper;

public class MainActivity extends AppCompatActivity {
    public Persona persona = new Persona();
    public Boolean masculino;
    public String ciudad = "";
    EditText textpass, textemail, textidentificacion, textedad, textapellido, textname;
    TextView text_fecha_nacimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button botonAceptar = (Button) findViewById(R.id.button_aceptar);
        final Button botonCancelar = (Button) findViewById(R.id.buttom_cancel);
        final Button botoncalendar = (Button) findViewById(R.id.boton_calendario);
        final RadioButton radioButtonFem = (RadioButton) findViewById(R.id.radio_button_femen);
        final RadioButton radioButtonMas = (RadioButton) findViewById(R.id.radio_button_masculino);

        masculino = radioButtonFem.isChecked();
        if (masculino) {
            Toast.makeText(getBaseContext(), "Masculino Seleccionado", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), "Femenino Seleccionado", Toast.LENGTH_LONG).show();
        }


        //Spinner
        Spinner spinner = (Spinner) findViewById(R.id.array_ciudad);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.laber_array_ciudad, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ciudad = parent.getItemAtPosition(position).toString();
                Toast.makeText(getBaseContext(), ciudad, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //*********** CALENDARIO
        botoncalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });

        /***************/

        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textname = (EditText) findViewById(R.id.edit_name);
                textapellido = (EditText) findViewById(R.id.edit_apellido);
                textedad = (EditText) findViewById(R.id.edit_edad);
                textidentificacion = (EditText) findViewById(R.id.edit_identificacion);
                textpass = (EditText) findViewById(R.id.edit_pass);
                textemail = (EditText) findViewById(R.id.edit_email);
                if (validar()) {
                    persona = new Persona(textname.getText().toString(), textapellido.getText().toString(), Integer.valueOf(textedad.getText().toString()),
                            "", textemail.getText().toString(), textpass.getText().toString(),
                            text_fecha_nacimiento.getText().toString(), masculino,
                            ciudad, textidentificacion.getText().toString());
                    if (_saveUser()) {
                        Intent intent = new Intent(getApplicationContext(), PreguntaActivity.class);
                        intent.putExtra("name_user", persona.getNombre());
                        startActivityForResult(intent, 0);
                    }
                }
            }
        });
        botonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    @SuppressLint("ShowToast")
    public Boolean validar() {
        if (textname == null || textname.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese el nombre", Toast.LENGTH_LONG);
            return false;
        }
        if (textapellido == null || textapellido.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese el Apellido", Toast.LENGTH_LONG);
            return false;
        }
        if (textedad == null || textedad.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese la Edad", Toast.LENGTH_LONG);
            return false;
        }
        if (textidentificacion == null || textidentificacion.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese el su identificación", Toast.LENGTH_LONG);
            return false;
        }
        if (textpass == null || textpass.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese el password", Toast.LENGTH_LONG);
            return false;
        }
        if (textemail == null || textemail.getText().length() == 0) {
            Toast.makeText(getBaseContext(), "Ingrese el email", Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    public void onRadioButtonClicked(View view) {
        Boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.radio_button_masculino:
                if (checked) {
                    masculino = true;
                    Toast.makeText(getBaseContext(), "Sexo Masculino Seleccionado", Toast.LENGTH_LONG);
                }
                break;
            case R.id.radio_button_femen:
                if (checked) {
                    masculino = false;
                    Toast.makeText(getBaseContext(), "Sexo Femenino Seleccionado", Toast.LENGTH_LONG);
                }
                break;
        }
    }

    public void showDatePickerDialog(View v) {
        text_fecha_nacimiento = (TextView) findViewById(R.id.edit_fecha_nacimiento);
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final String selectDate = dayOfMonth + "-" + month + "-" + year;
                text_fecha_nacimiento.setText(selectDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "Calendario");
    }

    private Boolean _saveUser() {
        MyOpenHelper myOpenHelper = new MyOpenHelper(this);
        final SQLiteDatabase db = myOpenHelper.getWritableDatabase();
        if (db != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("nombre", persona.getNombre());
            contentValues.put("user", persona.getEmail());
            contentValues.put("clave", persona.getPassword());
            contentValues.put("identificacion", persona.getIdentificacion());
            contentValues.put("apellido", persona.getApellido());
            db.insert("usuario", null, contentValues);
            Toast.makeText(MainActivity.this, "Usuario " + textname.getText().toString() + " Ingresado con éxito", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}