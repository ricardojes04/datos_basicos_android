package com.example.datosbasicos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datosbasicos.Model.Usuario;
import com.example.datosbasicos.SQLLite.MyOpenHelper;

import org.w3c.dom.Text;

public class SearchUserActivity extends AppCompatActivity {
    private TextView name_person;
    private TextView name_user;
    private TextView last_name_user;
    private EditText edit_identificacion;
    private Cursor c;
    private SQLiteDatabase db;
    private String sql = "SELECT u.user, u.nombre, u.identificacion, u.apellido FROM usuario u WHERE u.identificacion =?";
    private String[] args;
    private MyOpenHelper myOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        //BUTTON
        ImageButton button_search = (ImageButton) findViewById(R.id.btn_search);
        Button button_clean = (Button) findViewById(R.id.btn_clean_user);
        Button button_modify = (Button) findViewById(R.id.btn_modify_user);
        Button button_delete = (Button) findViewById(R.id.btn_delete_user);

        edit_identificacion = (EditText) findViewById(R.id.edit_identificacion_search);
        name_person = (TextView) findViewById(R.id.textView_name_person);
        name_user = (TextView) findViewById(R.id.textView_name_user);
        last_name_user = (TextView) findViewById(R.id.textView_apellido_user);
        /// OPEN DB
        myOpenHelper = new MyOpenHelper(this);


        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    args = new String[]{edit_identificacion.getText().toString()};
                    db = myOpenHelper.getWritableDatabase();
                    if (db != null) {
                        consultDB();
                        c.close();
                        db.close();
                    } else {
                        Toast.makeText(getBaseContext(), "Intente nuevamente \uD83D\uDE22", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        button_clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadView();
            }
        });

        button_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (validateUser()) {
                        System.out.println("identificacion" + edit_identificacion.getText().toString());
                        Intent intent = new Intent(getApplicationContext(), UpdateUserActivity.class);
                        intent.putExtra("identificacion_user", edit_identificacion.getText().toString());
                        startActivityForResult(intent, 0);
                    }
                }
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (validateUser()) {
                        deleteUser();
                    }
                }
            }
        });
    }

    private void consultDB() {
        c = db.rawQuery(sql, args);
        if (c != null) {
            c.moveToFirst();
            do {
                name_person.setText(c.getString(c.getColumnIndex("nombre")));
                last_name_user.setText(c.getString(c.getColumnIndex("apellido")));
                name_user.setText(c.getString(c.getColumnIndex("user")));
            } while (c.moveToNext());
        } else {
            Toast.makeText(getBaseContext(), "No existen datos de usuario \uD83D\uDE15", Toast.LENGTH_LONG).show();
        }
    }

    private void deleteUser() {
        db = myOpenHelper.getWritableDatabase();
        db.delete("usuario", "identificacion=?", args);
        db.close();
        loadView();
        Toast.makeText(getBaseContext(), "Usuario Eliminado con éxito \uD83D\uDE0E", Toast.LENGTH_LONG).show();
    }

    private void loadView() {
        name_person.setText("");
        name_user.setText("");
        last_name_user.setText("");
        edit_identificacion.setText("");
    }

    private Boolean validate() {
        if (edit_identificacion.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "Ingrese una identificación \uD83D\uDE15", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private Boolean validateUser() {
        if (name_user.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "No existen datos de usuario \uD83D\uDE15", Toast.LENGTH_LONG).show();
            return false;
        }
        if (name_person.getText().toString().isEmpty()) {
            Toast.makeText(getBaseContext(), "No existen datos de usuario \uD83D\uDE15", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}